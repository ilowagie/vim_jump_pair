" Jump-pair
"   A Vim plugin by Inigo Lowagie (inigo@lowagie.com)
"
" NewJumpPair(pat1<, pat2>) creates a new jump pair
" a jump pair is a pair of patterns that, when inserted, will result in the
" cursor jumping in between them and adding a jump marker after them.
" The jump marker can be jumped to when content between pair is inserted.

if exists('g:loaded_jumpPair') | finish | endif

let s:save_cpo = &cpo
set cpo&vim

function NewJumpPair(pat1, ...)
    if a:0 > 1
        echo "too many arguments passed to NewJumpPair"
    endif

    let l:pat2 = get(a:, 1, -1)
    if l:pat2 == -1
        let l:pat2 = a:pat1
    endif
    let l:jblen = strlen(pat2) + 3

    execute "imap <nowait> " . a:pat1 . l:pat2 . " " . a:pat1 . l:pat2 . "<++><Esc>" . l:jblen . "hi"

endfunction

execute "imap " . get(g:, 'JumpPair_jumpKey', '<C-j>') . ' <Esc>/<++><Enter>"_c4l'
execute "nmap " . get(g:, 'JumpPair_jumpKey', '<C-j>') . ' <Esc>/<++><Enter>"_c4l'

execute "imap " . get(g:, 'JumpPair_insertMarkerKey', '<C-l>') . ' <++>'

let &cpo = s:save_cpo
unlet s:save_cpo

let g:loaded_jumpPair = 1
